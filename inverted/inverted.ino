#define sel1 38
#define sel2 39
#define rst_ 40
#define clk 12
int theta,thetaold=0,thetadash;
byte theta1,theta2;
int alpha,alphaold=0,alphadash;
int K;
int temp = 250;
byte alpha1, alpha2;

void setup() {
  pinMode(sel1, OUTPUT);
  pinMode(sel2, OUTPUT);
  pinMode(rst_, OUTPUT);
  pinMode(clk, OUTPUT);

  TCCR1B=0x01;
  analogWrite(clk, 127);

  digitalWrite(rst_, LOW);
  delay(1000);
  digitalWrite(rst_, HIGH);

  DDRC=0b00000000;

  Serial.begin(9600);

}

void loop() {
  digitalWrite(sel1, HIGH);
  digitalWrite(sel2, LOW);
  alpha1=PINC;
  theta1=PINA;
  digitalWrite(sel1, LOW);
  digitalWrite(sel2, LOW);
  alpha2=PINC;
  theta2=PINA;
  alpha=word(alpha2, alpha1);
  theta=word(theta2, theta1);
  thetadash = (theta - thetaold);
  thetaold = theta;
  alphadash = (alpha - alphaold) ;
  alphaold = alpha;
  //K =  -(((-6.3246)*theta) + ((87.605)*alpha) + ((-3.32861)*thetadash) + ((11.3198)*alphadash));
  K =  -((( -2.8284)*theta) + ((51.6022)*alpha) + ((-1.6589)*thetadash) + ((6.2670)*alphadash));
  //      -2.0000   47.3356   -1.3900    5.6471
  //-2.8284   51.6022   -1.6589    6.2670
  // -6.3246   87.6051   -3.3286   11.3198


/*
 Serial.print(alpha);
 Serial.print(" ");
 
 Serial.print(theta);
 Serial.print(" ");
 
 Serial.print(alphadash);
 Serial.print(" ");
 
 Serial.print(thetadash);
 Serial.println(" ");
 */
 
 K=K/2;
 //Serial.println(K);
 
  if(K>temp)
  {K =temp;}
  if(K<-(temp))
  {K =-(temp);}
 
  if(K>0)
  {
  analogWrite(8,0);
  analogWrite(9,abs(K));
  }
  if(K<0)
  {
  analogWrite(8,abs(K));
  analogWrite(9,0);
  }
//  Serial.println(theta);
  delay(1);
 // Serial.println(alpha);
  //Serial.println(theta);
}
